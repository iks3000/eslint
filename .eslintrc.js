module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        'airbnb-base',
    ],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module',
    },
    overrides: [
        {
            files: ["*.js"],
            rules: {
                indent: "off",
                semi: "off",
                quotes: "off",
                "no-console": "off",
                "import/prefer-default-export": "off",
                "quote-props": "off",
                "no-trailing-spaces": "off",
                "max-classes-per-file": "off",
                "class-methods-use-this": "off",
                "comma-dangle": "off",
                "consistent-return": "off",
                "no-multiple-empty-lines": "off",
            },
        },
    ],
};
