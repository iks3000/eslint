import { hamburger } from "./hamburger";
import {
    // eslint-disable-next-line no-unused-vars
    events, changeContentStandard, changeContentAdvance, remove,
} from "./join-us-section";
import { validate } from "./email-validator";

hamburger();
events();
changeContentStandard();
// changeContentAdvance();
validate();
// remove();
