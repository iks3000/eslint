
const events = () => {
    window.addEventListener("load", () => {
        console.log("Page loaded!");

        const joinSection = document.querySelector("#join");
        if (joinSection !== null) joinSection.style.display = "block";

        const formDetails = document.querySelector("#formSubscribe");
        const inputEmail = document.querySelector("#inputSubscribe");
        if (inputEmail !== null) {
            formDetails.addEventListener("submit", (e) => {
                e.preventDefault();
                // eslint-disable-next-line template-curly-spacing
                console.log(`Email: ${ inputEmail.value }`);
                inputEmail.value = "";
            }, false);
        }
    });
};

class Creator {
    constructor(type, title, button) {
        this.type = type;
        this.title = title;
        this.button = button;
    }
}

class SectionCreator {
    create(type) {
        if (type === "standard") return new Creator(type, "Join Our Program", "Subscribe");
        if (type === "advanced") return new Creator(type, "Join Our Advanced Program", "Subscribe to Advanced");
    }
}

const obj = new SectionCreator();
const getTitle = document.querySelector("#joinTitle");
const getButton = document.querySelector("#joinButton");

function changeContentStandard() {
    window.addEventListener("load", () => {
        getTitle.innerHTML = obj.create("standard").title;
        getButton.innerHTML = obj.create("standard").button;
    });
}

function changeContentAdvance() {
    window.addEventListener("load", () => {
        getTitle.innerHTML = obj.create("advanced").title;
        getButton.innerHTML = obj.create("advanced").button;
    });
}

const remove = () => {
    document.querySelector("#join").remove();
};

export {
    events, changeContentStandard, changeContentAdvance, remove,
};
