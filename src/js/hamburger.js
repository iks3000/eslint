const hamburger = () => {
    const hamburgerEl = document.querySelector(".hamburger");
    const navMenu = document.querySelector(".navList");

    function mobileMenu() {
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
    }

    hamburgerEl.addEventListener("click", mobileMenu);

    const navLink = document.querySelectorAll(".navLink");

    function closeMenu() {
        hamburger.classList.remove("active");
        navMenu.classList.remove("active");
    }

    navLink.forEach((nav) => nav.addEventListener("click", closeMenu));
};

export { hamburger };
